---
title: Shakebook v0.2.2.0 Released!
author: Daniel Firth
date: 2020-05-06
tags: [shakebook, release]
description: Shakebook v0.2.2.0
---

This is an interim release in advance of shakebook v0.3, as we make several
huge architecture changes. We introduce new library
[shake-plus](https://hackage.haskell.org/package/shake-plus), which replaces
the standard shake API with path safe versions. `shake-plus` is still a little
rough but it's made life considerably easier not having to deal with raw
`FilePath`s everywhere.

<!--more-->

* Depend on new experimental library
  [shake-plus](https://hackage.haskell.org/package/shake-plus), that includes
re-exports of the Shake API based on the
[path](https://hackage.haskell.org/package/path) library for well-typed paths
and the [within](https://hackage.haskell.org/package/within) library which
introduces the `Within` type for representing a `Path` within a `Path`.
* Zipper functionality moved to external library
  [zipper-extra](https://hackage.haskell.org/package/zipper-extra).
* `Shakebook` and `ShakebookA` dropped in favour of `ShakePlus` and `RAction`
   from `shake-plus`.
