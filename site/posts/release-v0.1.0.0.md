---
title: Shakebook v0.1.0.0 Released!
author: Daniel Firth
date: 2020-04-12
tags: [shakebook, release]
description: Shakebook v0.1.0.0
---

This is a first preview of Shakebook with an example blog post demonstrating
the capabilities. Shakebook is just a template repository containing
opinionated ways to set up a documentation site using Haskell and Shake. You
can fork the repository and add/remove whatever you want.

<!--more-->

## Changelog

* Shake static site application that can export technical documentation both to
  HTML and to PDF using [pandoc](https://pandoc.org)
* Comes with a [nix](https://nixos.org/nix/) shell with full
* [LaTeX](https://www.latex-project.org/) and video rendering capabilities.
* Supports user configuration of table of contents via the `Shakefile.hs`
* Supports additional compilation units via [shake](https://shakebuild.com).
* Features two examples - one video rendering example with
  [reanimate](https://hackage.haskell.org/package/reanimate) and one generated
image using [R](https://www.r-project.org/) using
[inline-r](https://hackage.haskell.org/package/inline-r).
* Supports a blog section with tags, links to tag filtered pages and links to
  month filtered pages.
* Includes [bootstrap](https://getbootstrap.com/) and
  [fontawesome](https://fontawesome.com/) Supports
* [MathJax](https://www.mathjax.org/) and code syntax highlighting via pandoc's
  highlighting engine.  Features an example documentation section containing
the documentation for Shakebook itself.
* Supports [Atom](https://validator.w3.org/feed/docs/atom.html) feed generation
  from blog data.

Some features are undocumented but will be soon. Have fun!
