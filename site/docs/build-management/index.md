---
title: Build Management
description: Updating the build plans and dealing with updates to Shakebook
---

# Build Management

This chapter deals with how to manage shakebook's build systems to add your own
dependencies and packages.
