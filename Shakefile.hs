{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Shakefile where

import           Control.Lens                 hiding ((:<), Context)
import           Data.Aeson.With
import           Data.List.Split
import qualified Data.IxSet.Typed             as Ix
import           Data.Text.Time
import           Path.Extensions
import           RIO                          hiding (view)
import qualified RIO.ByteString.Lazy          as LBS
import qualified RIO.HashMap                  as HM
import           RIO.List
import           RIO.List.Partial
import           RIO.Partial
import qualified RIO.Text                     as T
import           RIO.Time
import           Shakebook

---Config-----------------------------------------------------------------------

siteTitle :: Text
siteTitle = "Shakebook"

-- The BaseURL of your site, mainly used for Atom Feeds
baseUrl :: Text
baseUrl = "https://shakebook.site"

-- The title of your Atom Feed.
feedTitle :: Text
feedTitle = "Shakebook Feed"

-- The output folder 'public', which is picked up by gitlab-ci.
outputFolder :: Path Rel Dir
outputFolder = $(mkRelDir "public")

-- The source folder 'site', containing your site source.
sourceFolder :: Path Rel Dir
sourceFolder = $(mkRelDir "site")

-- The golden folder 'golden', use to test against any changes you make.
goldenFolder :: Path Rel Dir
goldenFolder = $(mkRelDir "golden")

-- Posts per page
postsPerPage :: Int
postsPerPage = 5

-- Amount of recent posts to display
numRecentPosts :: Int
numRecentPosts = 5

-- Numer of page numbers to display around this one.
numPageNeighbours :: Int
numPageNeighbours = 2

mySocial :: [Value]
mySocial = []

-- The table of contents, this is a RoseTree. A head element a followed by a list of
-- subsections.
tableOfContents :: Cofree [] (Path Rel File)
tableOfContents =
  $(mkRelFile "docs/index.md") :< [
    $(mkRelFile "docs/getting-started/index.md") :< []
  , $(mkRelFile "docs/content-and-customization/index.md") :< [
      $(mkRelFile "docs/content-and-customization/markdown-table-of-contents.md") :< []
    , $(mkRelFile "docs/content-and-customization/image-compilation-units.md") :< []
    ]
  , $(mkRelFile "docs/build-management/index.md") :< [
      $(mkRelFile "docs/build-management/updating-build-plan.md") :< []
    ]
  ]

-- The animation format for reanimate videos
animationFormat :: String
animationFormat = ".mp4"

-- Animation options for reanimate. Default setting is to use imagemagick on low settings.
animationOptions :: [String]
animationOptions = ["--format", drop 1 animationFormat, "--raster", "convert", "--preset", "quick"]

--- Custom Build Rules ---

fromHaskell :: (MonadThrow m, MonadAction m) => [String] -> Within Rel (Path Rel File) -> m ()
fromHaskell opts out = do
  src <- blinkAndMapM sourceFolder withHsExtension out
  command_ [] "runhaskell" $ (toFilePath . fromWithin $ src) : opts

buildAnimation :: (MonadThrow m, MonadAction m) => Within Rel (Path Rel File) -> m ()
buildAnimation out = fromHaskell (["render", "-o", toFilePath . fromWithin $ out] ++ animationOptions) out

buildRPlot :: (MonadThrow m, MonadAction m) => Within Rel (Path Rel File) -> m ()
buildRPlot out = fromHaskell ["-o", toFilePath . fromWithin $ out] out

buildPDF :: (MonadThrow m, MonadAction m) => Cofree [] (Path Rel File) -> Path Rel File -> Within Rel (Path Rel File) -> m ()
buildPDF toc meta out = do
  k <- mapM (readMDFileIn defaultMarkdownReaderOptions sourceFolder) toc
  a <- readMDFileIn defaultMarkdownReaderOptions sourceFolder meta
  z <- replaceUnusableImages [".mp4"] (defaultVideoReplacement baseUrl) $ foldr (<>) a $ progressivelyDemoteHeaders k
  needPandocImagesIn outputFolder z
  let z' = prefixAllImages outputFolder z
  makePDFLaTeX defaultLatexWriterOptions z' >>= LBS.writeFile (toFilePath $ fromWithin out)

--- Custom Fields ---

withNavCover :: Value -> Value
withNavCover = withStringField "nav-class" "td-navbar-cover"


main :: IO ()
main = runSimpleShakePlus $ do

  want ["all"]

  readMDC <- newCache $ loadMarkdownAsJSON defaultMarkdownReaderOptions defaultHtml5WriterOptions

  postsIx <- newCache $ postIndex $ fmap defaultEnrichPost . readMDC

  postsZ <- newCache $ postsIx >=> postZipper

  blogNav <- newCache $ \fp -> do
    xs <- postsIx fp
    return $ genBlogNavbarData "Blog" "/posts" defaultPrettyMonthFormat defaultMonthUrlFragment xs

  blogIndexPageData <- newCache $ \fp -> do
    xs <- postsIx fp
    genIndexPageData (Ix.toList xs) "Posts" ("/posts/pages/" <>) postsPerPage

  blogTagIndexPageData <- newCache $ \fp -> do
    xs <- postsIx fp
    k <- forM (Ix.groupDescBy xs) $ \(Tag t, ys) -> do
      z <- genIndexPageData ys ("Posts tagged " <> t) (("/posts/tags/" <> t <> "/pages/") <>) postsPerPage
      return (t, z)
    return $ HM.fromList k

  blogMonthIndexPageData <- newCache $ \fp -> do
    xs <- postsIx fp
    k <- forM (Ix.groupDescBy xs) $ \(YearMonth (y, m), ys) -> do
      let t' = UTCTime (fromGregorian y m 1) 0
      z <- genIndexPageData ys
                      (("Posts from " <>) . defaultPrettyMonthFormat $ t')
                      (("/posts/months/"  <> defaultMonthUrlFormat t' <> "/pages/") <>) postsPerPage
      return (defaultMonthUrlFormat t', z)
    return $ HM.fromList k

  let myPosts = ["posts/*.md"] `within` sourceFolder

      s'      = (`within` sourceFolder)
      o'      = (`within` outputFolder)

      myBuildPage tmpl v out = do
        rs <- postsZ myPosts
        let v' = withHighlighting pygments
               . withSocialLinks mySocial
               . withSiteTitle siteTitle
               . withRecentPosts (take numRecentPosts (unzipper rs)) $ v
        buildPageActionWithin (s' tmpl) v' out

      myBuildBlogPage tmpl v out = do
        k <- blogNav myPosts
        myBuildPage tmpl (withJSON k v) out

      myBuildPostListPage z out = do
        let v = extract . extendPageNeighbours numPageNeighbours $ z
        myBuildBlogPage $(mkRelFile "templates/post-list.html") v out

  o' ("animations//*" ++ animationFormat) %^> buildAnimation

  o' "plots//*.png" %^> buildRPlot

  o' "book.pdf" %^> buildPDF tableOfContents $(mkRelFile "index.md")

  o' "index.html" %^> \out -> do
    src <- blinkAndMapM sourceFolder withMdExtension out
    v   <- readMDC src
    let v' = withNavCover v
    myBuildPage $(mkRelFile "templates/index.html") v' out

  o' "posts/*.html" %^> \out -> do
    src <- blinkAndMapM sourceFolder withMdExtension out
    xs <- postsZ myPosts
    xs' <- seekOnThrow viewSrcPath (T.pack . toFilePath . extract $ src) xs
    myBuildBlogPage $(mkRelFile "templates/post.html") (toJSON $ extract xs') out

  toc' <- mapM (mapM withHtmlExtension) $ fmap o' tableOfContents
  void . sequence . flip extend toc' $ \xs -> (toFilePath <$> extract xs) %^> \out -> do
    let getDoc = readMDC <=< blinkAndMapM sourceFolder withMdExtension
    ys <- mapM getDoc toc'
    zs <- mapM getDoc $ (fmap extract . unwrap $ xs)
    v  <- getDoc out
    let v' = withJSON (genTocNavbarData ys) . withSubsections zs $ v
    myBuildPage $(mkRelFile "templates/docs.html") v' out

  o' "posts/index.html" %^> copyFileChangedWithin (o' $(mkRelFile "posts/pages/1/index.html"))

  o' "posts/pages/*/index.html" %^> \out -> do
    let n = (+ (-1)) . read . (!! 2) . splitOn "/" . toFilePath . extract $ out
    xs <- blogIndexPageData myPosts
    myBuildPostListPage (seek n xs) out

  o' "posts/tags/*/index.html" %^> \out -> do
    let t = (!! 2) . splitOn "/" . toFilePath . extract $ out
    i <- parseRelFile $ "posts/tags/" <> t <> "/pages/1/index.html"
    copyFileChangedWithin (o' i) out

  o' "posts/tags/*/pages/*/index.html" %^> \out -> do
    let t = T.pack          . (!! 2) . splitOn "/" . toFilePath . extract $ out
    let n = (+ (-1)) . read . (!! 4) . splitOn "/" . toFilePath . extract $ out
    xs <- blogTagIndexPageData myPosts
    case HM.lookup t xs of
      Nothing -> logError $ "Attempting to lookup non-existant tag " <> display t
      Just x  -> myBuildPostListPage (seek n x) out

  o' "posts/months/*/index.html" %^> \out -> do
    let t = (!! 2) . splitOn "/" . toFilePath . extract $ out
    i <- parseRelFile $ "posts/months/" <> t <> "/pages/1/index.html"
    copyFileChangedWithin (o' i) out

  o' "posts/months/*/pages/*/index.html" %^> \out -> do
    let t = T.pack           . (!! 2) . splitOn "/" . toFilePath . extract $ out
    let n = (+ (-1)) . read  . (!! 4) . splitOn "/" . toFilePath . extract $ out
    xs <- blogMonthIndexPageData myPosts
    case HM.lookup t xs of
      Nothing -> logError $ "Attempting to lookup non-existant month " <> displayShow t
      Just x  -> myBuildPostListPage (seek n x) out

  o' ["css//*", "js//*", "webfonts//*", "images//*"] |%^> \out ->
    copyFileChangedWithin (blinkLocalDir sourceFolder out) out

  o' "feed.xml" %^> \x -> do
    xs <- postsZ myPosts
    buildFeed feedTitle baseUrl (unPost <$> unzipper xs) (fromWithin x)

  (["*//*"] `within` goldenFolder) |%^> \out ->
    copyFileChangedWithin (blinkLocalDir outputFolder out) out

  phony "all" $ need ["pdf", "animations", "docs", "feed", "index", "by-month-index",  "plots", "posts", "post-index", "by-tag-index", "statics"]

  let simplePipeline f = getDirectoryFiles sourceFolder >=> mapM f >=> needIn outputFolder
      verbatimPipeline = simplePipeline return

  phony "animations" $ simplePipeline (replaceExtension animationFormat) ["animations//*.hs"]

  phony "feed" $ needIn outputFolder [$(mkRelFile "feed.xml")]

  phony "pdf" $ needIn outputFolder [$(mkRelFile "book.pdf")]

  phony "plots" $ simplePipeline (replaceExtension ".png") ["plots//*.hs"]

  phony "statics" $ verbatimPipeline ["css//*", "js//*", "webfonts//*", "images//*"]

  phony "index" $ needIn outputFolder [$(mkRelFile "index.html")]

  phony "post-index" $ do
    ps <- blogIndexPageData myPosts
    fs <- defaultPagePaths [1..size ps]
    needIn (outputFolder </> $(mkRelDir "posts")) ($(mkRelFile "index.html") : fs)

  phony "by-tag-index" $ do
     ps <- blogTagIndexPageData myPosts
     void $ flip HM.traverseWithKey ps $ \t z -> do
       u  <- parseRelDir $ T.unpack t
       fs <- defaultPagePaths [1..size z]
       let tagFolder = outputFolder </> $(mkRelDir "posts/tags") </> u
       needIn tagFolder ($(mkRelFile "index.html") : fs)

  phony "by-month-index" $ do
     ps <- blogMonthIndexPageData myPosts
     void $ flip HM.traverseWithKey ps $ \t z -> do
       u  <- parseRelDir $ T.unpack t
       fs <- defaultPagePaths [1..size z]
       let monthFolder = outputFolder </> $(mkRelDir "posts/months") </> u
       needIn monthFolder ($(mkRelFile "index.html") : fs)

  phony "docs" $
    mapM withHtmlExtension tableOfContents >>= needIn outputFolder

  phony "posts" $ simplePipeline withHtmlExtension ["posts/*.md"]

  phony "clean" $ do
    logInfo $ "Cleaning files in " <> displayShow outputFolder
    removeFilesAfter outputFolder ["//*"]

  phony "test" $ do
    xs <- getDirectoryFilesWithinIO' (["*//*.html"] `within` goldenFolder)
    need ["clean"]
    need ["all"]
    forM_ xs $ \x -> do
      let y = blinkLocalDir outputFolder x
      let x' = toFilePath . fromWithin $ x
      let y' = toFilePath . fromWithin $ y
      a <- LBS.readFile x'
      b <- LBS.readFile y'
      case a == b of
        True  -> logInfo $ displayShow $ x' <> " and " <> y' <> " are the same."
        False -> logError $ displayShow $ x' <> " and " <> y' <> " are different."

  phony "golden" $ do
    need ["all"]
    getDirectoryFiles outputFolder ["*//*"] >>= needIn goldenFolder 
