{ sources ? import ./nix/sources.nix {}
, pkgs ? import sources.nixpkgs {} }:

let
  hsPkgs = import ./nix/default.nix;
in
  hsPkgs.shellFor {
    withHoogle = true;

    packages = ps: with ps; [shakebook-site];
    buildInputs = with pkgs.haskellPackages;
    [ ghcid
      hlint
      stylish-haskell
      wai-app-static
      pkgs.blender
      pkgs.ffmpeg-full
      pkgs.imagemagick
      pkgs.R
      pkgs.pkg-config
      pkgs.shake
      pkgs.texlive.combined.scheme-full
      pkgs.zlib.out
    ];

    LD_LIBRARY_PATH="${pkgs.zlib.out}/lib";
    exactDeps = true;
  }
