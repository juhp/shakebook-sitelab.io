# Shakebook

Shakebook is a static site generator powered by [slick](https://github.com/ChrisPenner/slick).

You will need the [nix](https://nixos.org/nix/) package manager to install dependencies. You
should also install [cachix](https://cachix.org) to speed up development. You can use the
shakebook cache by running

```
cachix use shakebook
```

## Deploying The Site

Just run shake from within the nix shell.

```
nix-shell --command shake
```

The output will appear in the `public/` folder.

## Serving The Site

Just run

```
nix-shell --command 'warp -d public'
```

and navigate to localhost:3000
